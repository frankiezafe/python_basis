# install pip: 
# - linux: sudo apt install python-pip
# - mac: https://stackoverflow.com/questions/17271319/how-do-i-install-pip-on-macos-or-os-x#18947390

# install https://github.com/hardikvasa/google-images-download
# sudo pip install google_images_download

#importing the library
import json
import os
from google_images_download import google_images_download

search_term = 'zuckerberg meme'

################### GATHERING ###################

#class instantiation
response = google_images_download.googleimagesdownload()

#creating list of arguments
arguments = {"keywords":search_term,"limit":20,"print_urls":True,"type":"animated","extract_metadata":True}

#passing the arguments to the function
paths = response.download(arguments)

#printing absolute paths of the downloaded images
print(paths)

################### ANALYSIS ###################

# opening the log
current_folder = os.path.dirname(os.path.realpath(__file__))
log_file = os.path.join( current_folder, "logs", search_term + '.json' )
print( 'opening log:', log_file )
f = open( log_file )

# turning it into a json object
json_content = json.load(f)

# looping over results
rescount = 0
for result in json_content:
	print( 'result: ' + str(rescount) )
	for key in result:
		try:
			print( '\t' + str(key) + ' = ' + str(result[key]) )
		except:
			print( '\timpossible to read key: ' + str(key) )
	rescount += 1

f.close()