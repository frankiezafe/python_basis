'''
this script is a downloader for https://serpapi.com/playground?q=apple&ijn=0&tbm=isch
configure your image search and copy/paste the results (lower right panel) to a file called serpapi.json, next to this script
fidn and replace '"thumbnail": ,' with '"thumbnail": "",'
and change "images_results" for a list to a dict by replacing '[]' with a '{}'
that's the simpliest way
once done, run the script
'''

import os
import json
import requests
import shutil

EXPORT_FOLDER = "downloads/serpapi/"

if not os.path.isdir(EXPORT_FOLDER):
	os.makedirs(EXPORT_FOLDER)

with open('serpapi.json') as f:

	data = json.load(f)

	# query name:
	qname = data["search_parameters"]["engine"] + '_' + data["search_parameters"]["q"].replace(" ", "")
	folder = os.path.join( EXPORT_FOLDER, qname )
	if not os.path.isdir(folder):
		os.makedirs(folder)

	# getting images
	i = len([name for name in os.listdir(folder) if os.path.isfile(os.path.join(folder, name))])
	print( i )

	for k in data["images_results"]:
		url = data["images_results"][k]['original']
		file = str( i )
		while len( file ) < 6:
			file = '0' + file
		file += '_' + os.path.basename(url)
		print( "downloading ", url, "\n\tto ", file )
		resp = requests.get( url, stream=True )
		local_file = open( os.path.join( folder, file ), 'wb')
		resp.raw.decode_content = True
		shutil.copyfileobj(resp.raw, local_file)
		del resp
		i += 1