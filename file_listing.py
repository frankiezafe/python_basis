from os import walk

###################
## CONFIGURATION ##
###################

# folder to scan
path="/home/frankiezafe/Downloads"
# extensions to consider
extensions=[".mp4",".mov",".mkv"]

###################
##### PROCESS #####
###################

# scanning folder and sub-folders
# contains all files
allfiles=[]
# contains only files with the specified extension
selectedfiles=[]
for (dirpath, dirnames, filenames) in walk(path):
	# uncomment to check the variables
	#print(dirpath)
	#print(dirnames)
	#print(filenames)
	for f in filenames:
		for e in extensions:
			if f[-len(e):] == e:
				selectedfiles.append( dirpath+"/"+f )
				print(e, ">>>", f)
	allfiles.extend(filenames)

####################
###### OUTPUT ######
####################

#print(allfiles)
# printing the list as it is
print("UNSORTED LIST")
i = 0
for s in selectedfiles:
	print(i,s)
	i += 1

print("SORTED LIST")
selectedfiles.sort()
i=0
for s in selectedfiles:
	print(i,s)
	i += 1

# writing to file

output=open("selectedfiles.txt", "w")
for s in selectedfiles:
	output.write(s + '\n')
output.close()
