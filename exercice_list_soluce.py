
letter_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

words = ["Python","est","le","plus", "cool", "des","languages","de","programmation","de","l","univers"]

out = ''
for w in words:

    if out is not '':
        out += ', '
    array = ''

    for c in w:
        for i in range( 0, len( letter_list ) ):
            if letter_list[i] == c:
                if array is not '':
                    array += ', '
                array += str( i )
                break
    out += '[' + array + ']'

out = 'sentence = [' + out + ']'

print( out )

sentence = [[25, 60, 55, 43, 50, 49], [40, 54, 55], [47, 40], [51, 47, 56, 54], [38, 50, 50, 47], [39, 40, 54], [47, 36, 49, 42, 56, 36, 42, 40, 54], [39, 40], [51, 53, 50, 42, 53, 36, 48, 48, 36, 55, 44, 50, 49], [39, 40], [47], [56, 49, 44, 57, 40, 53, 54]]

out = ''
for w in sentence:
    for c in w:
        out += letter_list[c]
    out += ' '
print( out )


