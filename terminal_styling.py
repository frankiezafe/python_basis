import os
import time

# ascii art online:
####################
# text: http://www.network-science.de/ascii/
# image: https://www.ascii-art-generator.org/

def clear():
    os.system('cls' if os.name == 'nt' else 'clear')

clear()

# voir toutes les couleurs possibles

print( '\33[0;20;20mTEXT STYLES\n' )
for i in range( 0, 9 ):
    print( '\33[' + str(i) + ';20;20m \\33[' + str(i) + ';20;20m ****************************' )

print( '\n\n\33[0;20;20mFOREGROUND COLOR\n' )
for i in range( 0, 50 ):
    print( '\33[0;' + str(i) + ';20m \\33[0;' + str(i) + ';20m ****************************' )

print( '\n\n\33[0;20;20mBACKGROUND COLOR\n' )
for i in range( 0, 50 ):
    print( '\33[0;20;' + str(i) + 'm \\33[0;20m;' + str(i) + ' ****************************' )

# variables pour les couleurs

default = '\33[0;20;20m'
red_bg = '\33[0;20;41m'

while True:
    
    print( red_bg + """
;'',,''',;cc;,''';;'.... .              ..........
,',,,';lxOKO:.    'lc'.......',,,,'''.. .';;,;:;;:
,',,;oOKXXXX0kxdooxOOd;'::cccloooolcc:;...,:cllllo
;,,lkKXKKXXXNXXXXXK00kdccllllooooolcc::;...:lllllo
;,cOOl;,,lKNNNXXXXKkdxko::::clloollc::;;'.,llllloo
;;d0l.  ..oNWWNXXOc...,;;'....':l:'....,..:lcllloo
,:xO:    .:0NXXKO:     .:lc;;;::c:,'.....':cclcclo
,;dOd:'..l0KKKK00l.    'clllllllc;,::;;;',:cllccll
;,cxO0Oxk0K0000OOkc...',::lolcc:,,:c::;,';ccldlloo
;,,cxkkOkkOOOOOkkxdlllc;;cll:;,'':lc,'.';cclolcloo
,'',:oddddlodddxdddl:;'.;c:;,,'',::,'..;ccccllcloo
,''',,:cc,...'',clc,....,;;;,',::;;'..,:ccclolclol
,,''''',,,,,;,'.,;'....''',,;;:::;'..';:ccclolclol
;,'''',,,,,;,''''......;,'',;;;;,.....';ccclolcloo
\033[95m                                                                                                  
""" )
    i = input( default + '?' )
    print( 'Vous avez entrer: ' + i )
    print( '***************************' )
    time.sleep( 0.5 )
    print('''
      ___           ___           ___           ___     
     /\  \         /\  \         /\__\         /\  \    
    /::\  \       /::\  \       /::|  |       /::\  \   
   /:/\:\  \     /:/\:\  \     /:|:|  |      /:/\:\  \  
  /:/  \:\  \   /::\~\:\  \   /:/|:|__|__   /::\~\:\  \ 
 /:/__/_\:\__\ /:/\:\ \:\__\ /:/ |::::\__\ /:/\:\ \:\__\ \n
 \:\  /\ \/__/ \/__\:\/:/  / \/__/~~/:/  / \:\~\:\ \/__/
  \:\ \:\__\        \::/  /        /:/  /   \:\ \:\__\  
   \:\/:/  /        /:/  /        /:/  /     \:\ \/__/  
    \::/  /        /:/  /        /:/  /       \:\__\    
     \/__/         \/__/         \/__/         \/__/    

      ___           ___           ___           ___     
     /\  \         /\__\         /\  \         /\  \    
    /::\  \       /:/  /        /::\  \       /::\  \   
   /:/\:\  \     /:/  /        /:/\:\  \     /:/\:\  \  
  /:/  \:\  \   /:/__/  ___   /::\~\:\  \   /::\~\:\  \ 
 /:/__/ \:\__\  |:|  | /\__\ /:/\:\ \:\__\ /:/\:\ \:\__\ \n
 \:\  \ /:/  /  |:|  |/:/  / \:\~\:\ \/__/ \/_|::\/:/  /
  \:\  /:/  /   |:|__/:/  /   \:\ \:\__\      |:|::/  / 
   \:\/:/  /     \::::/__/     \:\ \/__/      |:|\/__/  
    \::/  /       ~~~~          \:\__\        |:|  |    
     \/__/                       \/__/         \|__|    

    ''')
    time.sleep( 4 )
    clear()
