import json

sample_number = 10000
json_path = "../javascript/json/testeur.json"
export_folder = "../puredata/configs/"

def parse_ae_features( features ):
	global track_name
	global pd_config
	# looping over layers
	for layer in features:
		track_name = layer["name"]
		#print layer["name"]
		pd_previous_value = 0
		pd_config[track_name] = []
		for feat in layer["feature_animations"]:
			if feat["property"] == "OPACITY":
				print "number of keys in", track_name, ":", len( feat["key_values"] )
				parse_opacity_key( feat["key_values"] )
		#print len(pd_config[track_name])

def parse_opacity_key( data ):
	global track_name
	global pd_config
	track = pd_config[track_name]
	for k in range( 0, len( data ) ):
		samplei = int( data[k]["start_frame"] * sample_multiplier )
		value = float( data[k]["data"][0] ) * 0.01
		if len(track) == 0:
			track.append(0)
		prev_value = track[ len(track) - 1 ]
		diff = value - prev_value
		diffi = samplei - len(track)
		#print "should add", diffi, prev_value, value
		while( samplei > len(track) ):
			prev_value += diff / diffi
			if prev_value < 0:
				prev_value = 0
			elif prev_value > 100:
				prev_value = 100
			#print "\tappend", prev_value, value
			track.append( prev_value )
		track.append( value )
		#print data[k]["start_frame"]
		#print data[k]["data"]

pd_config = {}
pd_previous_value = 0

f = open( json_path )
data = json.load( f )
sample_multiplier = 1

track_name = ""
for a in data:
	if a == "animation_frame_count":
		sample_multiplier = sample_number / float(data[a])
		#print "COUNT", data[a], sample_multiplier
	elif a == "features":
		parse_ae_features( data[a] )

counter = 0
for k in pd_config:
	#print k
	fname = export_folder + k
	'''
	if counter < 10:
		fname += "000"
	elif counter < 100:
		fname += "00"
	elif counter < 1000:
		fname += "0"
	fname += str( counter )
	'''
	dump = open( fname, "w" )
	for i in pd_config[k]:
		dump.write( str(i) + ";\n" )
	dump.close()
	counter += 1

