
#########################
######### tuple #########
#########################

var = (2,3,45)

print "\n++++++ tuple ++++++\n"
print "values:" 
print "\t" + str( var )
print "length:" 
print "\t" + str( len( var ) )

print( "\naccess by index loop" )
for i in range( 0, len( var ) ):
    print "\tvar[" + str( i ) + "] : " + str( i )

print( "\nforeach loop" )
for i in var:
    print "\t" + str( i )

try:
    var.append( 27 )
except:
    print "\nimpossible to append a value in a tuple"

try:
    var.remove( 0 )
except:
    print "\nimpossible to remove a value from a tuple"

########################
######### list #########
########################

var = [0,2,8,777,4,10,35,3]

print( "\n++++++ list ++++++\n" )
print "values:" 
print "\t" + str( var )
print "length:" 
print "\t" + str( len( var ) )

print( "\naccess by index loop" )
for i in range( 0, len( var ) ):
    print "\tvar[" + str( i ) + "] : " + str( var[ i ] )

print( "\nforeach loop" )
for i in var:
    print "\t" + str( i )

print "\nsorting a list"
var = sorted(var)
print "\t" + str( var )

print "\nappending a value in a list"
var.append( 27 )
print "\t" + str( var )

print "\nremoving a value from a tuple"
var.remove( 777 )
print "\t" + str( var )

print "\nsplitting a list in two list"
print "\t" + str( var[ : len( var )/2 ] ) + " & " +  str( var[ len( var )/2 : ] )

###############################
######### dictionnary #########
###############################

var = {
    "number" : 34,
    "kind" : "top cool",
    "idea" : None,
    "list" : [45,89,13,0,20039]
}

print( "\n++++++ dict ++++++\n" )
print "values:" 
print "\t" + str( var )
print "length:" 
print "\t" + str( len( var ) )

print( "\nkeys" )
keys = var.keys()
print "\t" + str( keys )

print( "\nvalues" )
values = var.values()
print "\t" + str( values )

print( "\naccess by keys loop" )
for i in range( 0, len( keys ) ):
    print "\tvar[" + str( keys[i] ) + "] : " + str( var[ keys[ i ] ] )

print( "\nforeach loop" )
for key in var:
    print "\t" + str( key ) + " : " + str( var[ key ] )

print( "\nsorting keys and access by keys loop" )
keys = sorted(keys)
for i in range( 0, len( keys ) ):
    print "\tvar[" + str( keys[i] ) + "] : " + str( var[ keys[ i ] ] )

