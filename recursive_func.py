reccount = 0

def recfunc( x, y ):
    
    global reccount

    print( "recfunc, call: " + str( reccount ) + " with args ( " + str(x) + ", " + str(y) + " )" )
    reccount += 1
    
    if y > 1:
        return recfunc( x * 10, y - 1 )
    else:
        reccount = 0
        return x
    pass

print( recfunc( 10, 10 ) )

print( recfunc( 10, 0 ) )

print( pow( 10, 10 ) )
