import random

letter_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

f = open( "file_read_write-256_characters.txt", 'w' )

letters = ''
for i in range( 0, 256 ):
    f.write( str(i) + " = " + chr(i) + '\r' )
    if ( i >= 48 and i <= 57 ) or ( i >= 65 and i <= 90 ) or ( i >= 97 and i <= 122 ):
        if letters is not '':
            letters += ', '
        letters += "'" + chr(i) + "'"
        
f.close()

print( letters )

word_len = (3,50)
char_ranges = [ (33,93), (97,126), (161,172), (191,255) ]

f = open( "file_read_write-words.txt", 'w' )

for i in range( 0, 500 ):
    letternum = word_len[0] + int( random.random() * (word_len[1]-word_len[0]) )
    for i in range( 0, letternum ):
        # picking a range:
        crid = int( random.random() * len( char_ranges ) )
        cr = char_ranges[ crid ]
        c = cr[0] + int( random.random() * (cr[1]-cr[0]) )
        f.write( chr(c) )
    f.write( '\n' )

f.close()
