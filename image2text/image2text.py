from PIL import Image

im = Image.open('black-panther.png')
rgb_im = im.convert('RGB')
width, height = im.size

f = open( "image2text.html", "w" )
f.write( "<html>\n" )
f.write( "<style> html { font-family: courier; } p { margin:0; padding:0; white-space:nowrap; } </style>\n" )
f.write( "<body>\n" )

txt = open( "ipsum.txt", "r" )
lines = txt.readlines()
y = 0
for l in lines:
	x = 0
	f.write( "<p>\n" )
	for letter in l:
		if x >= width:
			x = 0
			y += 1
			f.write( "</p><p>\n" )
		f.write(  '<span' )
		if y < height:
			r, g, b = rgb_im.getpixel((x, y))
			f.write(  ' style="background:rgb( '+str(r)+','+str(g)+','+str(b)+' )"' )
		f.write(  '>' + letter + '</span>' )
		x += 1
	f.write( "</p>\n" )
	y += 1

'''
for y in range( 0, height ):
	f.write( "<p>\n" )
	for x in range( 0, width ):
		r, g, b = rgb_im.getpixel((x, y))
		f.write(  '<span style="background:rgb( '+str(r)+','+str(g)+','+str(b)+' )">?</span>' )
	f.write( "</p>\n" )
'''

f.write( "</body></html>" )
