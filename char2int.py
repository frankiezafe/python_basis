c = chr(97)

print( 'char', c )

i = ord( 'a' )

print( 'int', i )

st = "ma chaine de caracteres"

for c in st:
    print( c, ord(c) )

for i in range( 0, 255 ):
	print( str(i) + " = " +  chr( i ) )